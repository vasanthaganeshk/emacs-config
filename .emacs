(setq column-number-mode t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (wombat)))
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa-stable" . "http://stable.melpa.org/packages/"))))
 '(package-selected-packages
   (quote
    (dockerfile-mode helm powerline yasnippet paredit markdown-mode multiple-cursors haskell-mode))))

;; remove toolbar and menubar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(set-default-font "SourceCodePro")

;; Haskell mode

(add-to-list 'load-path "/home/homonculus/Code/Projects/haskell-interactive-mode")
(require 'haskell-mode-autoloads)
(add-to-list 'Info-default-directory-list "/home/homonculus/Code/Projects/haskell-interactive-mode")

;; Hook for deleting trailing white-space
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;; Haskell interactive mode

(add-hook 'haskell-mode-hook 'interactive-haskell-mode)

;;;;;;;
(require 'package)
(package-initialize)

;; (add-to-list 'load-path "/home/homonculus/Code/Projects/lsp-mode")
;; (add-to-list 'load-path "/home/homonculus/Code/Projects/lsp-haskell")

;; (require 'lsp-mode)
;; (require 'lsp-haskell)

;; (with-eval-after-load 'lsp-mode
;;     (require 'lsp-flycheck))

;; (add-hook 'lsp-haskell #'lsp-mode)
(require 'powerline)
(powerline-default-theme)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; paredit
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook 'emacs-lisp-mode-hook 'paredit-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HELM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'helm-config)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-s") 'helm-occur)
(global-set-key (kbd "C-M-s") 'helm-grep-do-git-grep)
(helm-mode 1)
;; C-M-i helm-auto-completion

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
