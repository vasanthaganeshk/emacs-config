(deftheme solar-vasa
  "Created 2017-07-03.")

(custom-theme-set-variables
 'solar-vasa
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("melpa-stable" . "http://stable.melpa.org/packages/"))))
 '(package-selected-packages (quote (solazized powerline yasnippet paredit markdown-mode multiple-cursors haskell-mode)))
 '(custom-safe-themes (quote ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default))))

(custom-theme-set-faces
 'solar-vasa
 '(custom-link ((t (:inherit (link))))))

(provide-theme 'solar-vasa)
